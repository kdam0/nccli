{
  description = "My Go CLI Project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    devenv.url = "github:cachix/devenv";
  };

  outputs = inputs@{ flake-parts, nixpkgs, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devenv.flakeModule
      ];
      systems = nixpkgs.lib.systems.flakeExposed;
      perSystem = { config, self', inputs', pkgs, system, ... }: {

        packages.default = pkgs.hello;

        devenv.shells.go = {
          packages = [ config.packages.default ];
          languages.go.enable = true;
          enterShell = ''
            echo this is my go project shell.
          '';
        };
      };
    };
}
