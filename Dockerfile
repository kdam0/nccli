FROM golang:alpine as base

RUN apk add --update \
    ca-certificates \
    gcc \
    libc-dev

COPY . $GOPATH/src/gitlab.com/kdam0/nccli
WORKDIR $GOPATH/src/gitlab.com/kdam0/nccli

FROM base as test
RUN ["go", "test", "-cover"]

FROM base as builder
RUN go get -d -v
ARG opts
RUN env ${opts} go build -o /bin/nccli

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /bin/nccli /bin/nccli
CMD ["/bin/nccli"]
