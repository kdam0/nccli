#!/usr/bin/env bash

source nccli.env
# TODO: validate that $DOMAIN is provided.
API_USER=$API_USER \
  API_KEY=$API_KEY \
  ACCESS_IP=$ACCESS_IP \
  ./nccli "$@"
